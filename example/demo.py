
def jforms_demo(**kwargs):
    array = [
        [
            [

                {
                    'name': 'file-demo',
                    'label': "Add File Here",
                    'attribs': "",
                    'file': {

                    }
                },

                {
                    'name': "name",
                    'label': "Full Name",
                    'attribs': "autofocus",
                    'input': {
                        'placeholder': "Name",
                        'value': kwargs.get('default_name') or '',
                    }
                }
            ],
            [
                {
                    'name': "period",
                    'attribs': "required onchange='onChangePeriod();'",
                    'select': {
                        'choices': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ],
                        'selected': [""],
                    }
                },
                {
                    'name': "period1",
                    'attribs': "required onchange='onChangePeriod();'",
                    'select': {
                        'choices': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ],
                        'selected': [""],
                    }
                },

                {
                    'name': 'vehices',
                    'label': "Vehicles",
                    'inline': True,
                    'checkboxes': [
                        ("checkbox1", "C1", "opt1", True),
                        ("checkbox2", "C2", "opt2", True),
                        ("checkbox3", "C3", "opt3", False),
                    ]

                },
            ],
            [
                {
                    'name': 'gender',
                    'label': "Gender",
                    'inline': True,
                    'radios': [
                        ("radio1", "R1", "opt1", True),
                        ("radio2", "R2", "opt2", True),
                        ("radio3", "R3", "opt3", False),
                    ]

                },
            ],
        ],
        [
            [
                {
                    'name': "dob-ne",
                    'label': "Date of Birth (in B.S.)",
                    'attribs': "required",
                    'datepicker_np': {

                    }

                },
                {
                    'name': "dob-eng",
                    'label': "Date of Birth (in A.D.)",
                    'attribs': "required",
                    'datepicker': {

                    }

                },
            ],
        ],
        [
            [
                {
                    'name': "period",
                    'attribs': "required onchange='onChangePeriod();'",
                    'select': {
                        'choices': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ],
                        'selected': [""],
                    }
                },
                {
                    'name': "period1",
                    'attribs': "required onchange='onChangePeriod();'",
                    'select': {
                        'choices': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ],
                        'selected': [""],
                    }
                }
            ]
        ],
        [
            [
                {
                    'name': "TAB3",
                    'attribs': "required autofocus",
                    'input': {
                        'placeholder': "TAB3",
                        'value':  ''
                    }}], [
                {
                    'name': "period1", 'attribs': "required onchange='onChangePeriod();'",
                    'select': {
                        'choices': [
                            ('Anually', 'Anually'), ('Monthly', 'Monthly'), ], 'selected': [""], }}]],
        [
            [
                {
                    'name': "LAST", 'attribs': "required autofocus", 'input': {'placeholder': "last", 'value': '', }}], [
                {
                    'name': "Last", 'attribs': "required onchange='onChangePeriod();'",
                    'select': {
                        'choices': [
                            ('Anually', 'Anually'), ('Monthly', 'Monthly'), ], 'selected': [""], }},
                {'name': "Final", 'attribs': "required onchange='onChangePeriod();'",
                 'select': {'choices': [('Anually', 'Anually'), ('Monthly', 'Monthly'), ], 'selected': [""], }}]],
    ]
    return array
