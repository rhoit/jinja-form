#!./venv/bin/python

import flask
import demo as wtf_demo
from jforms import JForms

app = flask.Flask(__name__)

# Add JForms templating to current Flask's instance
JForms(app)

@app.route('/vendors/<path:filepath>')
def serve_vendor(filepath):
    return flask.send_from_directory('vendors', filepath)

@app.route('/')
def jforms_demo():
    return flask.render_template("base.djhtml", forms=wtf_demo.jforms_demo(default_name="user"))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, threaded=True, use_reloader=True, debug=True)
