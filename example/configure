#!/usr/bin/env bash

set -e

# * VENDORS
echo "* vendors"
mkdir -p ./vendors
cd ./vendors

function fetch {
    LINK="${NAME}.${EXT}"
    FILE="${NAME}-${VER}.${EXT}"
    echo "** ${FILE}"
    if test -e "$FILE"; then
        echo "   already exist"
        if [[ $(readlink $LINK) != "$FILE" ]]; then
            rm -f "$LINK"
            ln -s "$FILE" "$LINK"
            echo "   relinked:" readlink $LINK
        fi
    else
        curl -L "$URL" -o "$FILE"
        rm -f "$LINK"
        ln -s "$FILE" "$LINK"
    fi

    if [[ $1 == "map" ]]; then
        return
    fi

    URL_map=$(sed -n '$s/^.*# sourceMappingURL=\(.*map\).*$/\1/p' "$FILE")
    if [[ "$URL_map" != "" ]]; then
        EXT+=".map" URL="$(dirname $URL)/${URL_map}"
        fetch map
    fi
}

# ** jQuery
## https://jquery.com/
NAME="jquery" VER="3.5.1" EXT="min.js"
URL="https://code.jquery.com/${NAME}-${VER}.${EXT}"
fetch


# ** bootstrap
## https://getbootstrap.com/
NAME="bootstrap" VER="4.5.3" EXT="min.css"
URL="https://cdn.jsdelivr.net/npm/bootstrap@${VER}/dist/css/${NAME}.${EXT}"
fetch
EXT="bundle.min.js"
URL="https://cdn.jsdelivr.net/npm/bootstrap@${VER}/dist/js/${NAME}.${EXT}"
fetch


# ** font-awesome
## https://fontawesome.com/
NAME="font-awesome" VER="5.14.0" EXT="min.js"
URL="https://use.fontawesome.com/releases/v${VER}/js/all.js"
fetch


# *** bootstrap-datepicker
## https://github.com/uxsolutions/bootstrap-datepicker
echo "*** bootstrap-datepicker"
tbdate_VER="1.9.0"
if test -e 'bootstrap-datepicker'; then
    echo "   already exist"
else
    tbdate_ZIP="bootstrap-datepicker-${tbdate_VER}-dist.zip"
    test -e ${tbdate_ZIP} || curl -L "https://github.com/uxsolutions/bootstrap-datepicker/releases/download/v${tbdate_VER}/${tbdate_ZIP}" -o ${tbdate_ZIP}
    unzip ${tbdate_ZIP} -d 'bootstrap-datepicker'
fi


# ** nepali-date-picker
## https://github.com/leapfrogtechnology/nepali-date-picker
NAME="nepaliDatePicker" VER="2.0.1" EXT="min.css"
URL="https://unpkg.com/nepali-date-picker@${VER}/dist/${NAME}.${EXT}"
fetch
EXT="min.js"
URL="https://unpkg.com/nepali-date-picker@${VER}/dist/jquery.${NAME}.${EXT}"
fetch


# ** jinja-form
## https://gitlab.com/rhoit/jinja-form
echo "** jinja-form"
if test -e 'jinja-form'; then
    echo "   updating ... "
    (cd jinja-form; git pull)
else
    git clone --depth=1 "git@gitlab.com:rhoit/jinja-form.git"
    mkdir -p ../static/js
    ln -sf $PWD/jinja-form/jforms/static/js/jform.js            ../static/js/jform.js
    mkdir -p ../static/css
    ln -sf $PWD/jinja-form/jforms/static/css/jforms.css         ../static/css/jforms.css
    mkdir -p ../templates
    ln -sf $PWD/jinja-form/jforms/templates/jform.djhtml        ../templates/jform.djhtml
    ln -sf $PWD/jinja-form/jforms/templates/jform-macros.djhtml ../templates/jform-macros.djhtml
fi

# * VENV
echo "* virtual environment"
cd ../
PATH_venv="venv"
test -d "$PATH_venv" || python3 -m venv "$PATH_venv"
source "$PATH_venv/bin/activate"
pip install flask &&
pip install ./vendors/jinja-form
deactivate
