import setuptools

# Reference: Serious Python, Chapter 5.3
# Excellent answer on how to diagnose packaging problems
# https://stackoverflow.com/questions/50585246/pip-install-creates-only-the-dist-info-not-the-package

setuptools.setup(
    name           = "jform",
    packages       = ['jform'],
)
