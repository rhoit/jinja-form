#+TITLE: jinja-form
#+DATE: 2021 Feb 08, Monday


Jforms is a JSON-to-Form library for the Jinja2 templating language.

Check out the demo in example/ folder for examples.

* INSTALLATION

  Instruction installations for Jforms.

  #+HEADER: :exports both :eval no-export
  #+BEGIN_SRC sh :results output
    git clone https://gitlab.com/rhoit/jinja-form.git
    cd jinja-form
    pip install .
  #+END_SRC

* QUICKSTART

  You can start using Jforms with flask in 3 easy steps.

** 1. import jforms

   #+HEADER: :exports both :eval no-export
   #+BEGIN_SRC python :results output
     import flask
     import jforms import JForms
   #+END_SRC


** 2. inject Jform's templates to Flask app's jinja instance

   #+HEADER: :exports both :eval no-export
   #+BEGIN_SRC python :results output
     app = flask.Flask(__name__)
     JForms
     jform.wtf(app)
   #+END_SRC


** 3. Use Jforms as you like!

   #+HEADER: :exports both :eval no-export
   #+BEGIN_SRC python :results output
     @app.route('/users')
     def view_userLst():
         return flask.render_template(
             'jform.djhtml',
             forms  = jforms.users.User(**{
                 'uname'  : 'admin',
                 'groups' : [0],
             })
         )
   #+END_SRC
