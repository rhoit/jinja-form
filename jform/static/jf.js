function JForm(fid='wtf', cTab=0) {
    const eTabs   = document.getElementsByClassName("jf-tab")
    const eSteps  = document.getElementsByClassName("jf-step")
    const prevBtn = document.getElementById("jf-prevBtn")
    const nextBtn = document.getElementById("jf-nextBtn")

    this.inputTypes = "input, textarea, datalist, select"

    this.showTab = function (n) {
        let eTabN = eTabs[n]
        eTabs[cTab].classList.remove("active")
        eTabN.classList.add("active")

        let found = eTabN.getElementsByClassName('autofocus')
        if (found.length > 0) {
            found[0].focus()
        }

        eSteps[cTab].classList.remove("active")
        eSteps[n].classList.add("active")

        prevBtn.disabled = (n == 0)
        nextBtn.disabled = (n == eTabs.length - 1)
        cTab = n
    };


    this.shiftTab = function (direction=1, validate=true) {
        let newTab = cTab + direction
        if (0 < newTab && newTab >= eTabs.length ) { // last tab
            console.log("Index Out of Range")
            return
        }
        if (!this.validateForm(newTab)) return
        this.showTab(newTab)
    }

    this.validateForm = function () {
        // This function deals with validation of the form fields
        cTab = parseInt(cTab);
        let y, i, valid = true;
        y = eTabs[cTab].querySelectorAll(this.inputTypes);
        // clean-up classes representing invalid inputs
        document.querySelectorAll('.invalid-feedback').forEach(function (e) {
            e.remove()
        })
        document.querySelectorAll('.is-invalid-radio').forEach(function (e) {
            e.classList.remove('is-invalid-radio')
        })
        // A loop that checks every input field in the current tab:
        for (eInput of y) {
            eInput.classList.remove("is-invalid")
            // If a field is invalid...
            if (!eInput.reportValidity()) {
                eDivInvalid = document.createElement('div')
                eDivInvalid.classList.add('invalid-feedback', 'jf-no-print')
                eDivInvalid.innerText = eInput.validationMessage

                // Radios are multiple input tags - an exception
                if (eInput.type === 'radio' && !eInput.parentElement.parentElement.classList.contains('is-invalid-radio')){
                    eInput.parentElement.parentElement.append(eDivInvalid)
                    eInput.parentElement.parentElement.classList.add('is-invalid-radio')
                    eInput.parentElement.classList.add("is-invalid")
                } else if (eInput.type != 'radio') {
                    eInput.parentElement.append(eDivInvalid)
                    eInput.classList.add("is-invalid")
                }

                // add an "invalid" class to the tab and step:
                eTabs[cTab].className  += " invalid";
                eSteps[cTab].className += " invalid";
                // and set the current valid status to false:
                valid = false;
            } else {

            }
            $(".date-picker-ne").prop("readonly", false);
            // this.reportFieldValidity();
        }

        // If the valid status is true, mark the step as finished and valid and remove invalid if exists:
        if (valid) {
            eSteps[cTab].classList.remove("invalid");
            eTabs[cTab].classList.remove("invalid");
            eSteps[cTab].className += " finish";
        }

        return valid; // return the valid status
    };


    this.reportFieldValidity = function () {
        let cTabID = "jf-tab-" + cTab;
        let cTabElement = document.getElementById(cTabID);
        let cTabChildInputs = cTabElement.querySelectorAll(this.inputTypes);

        cTabChildInputs.forEach(input => input.reportValidity());
    };
}


function showImagePreview(event, inpField) {
    let previewImg = document.getElementById(inpField.id+'-img')
    previewImg.style.display = "unset"
    let file = inpField.files[0]
    let reader = new FileReader()
    reader.onload = function (event) {
        previewImg.src = event.target.result
    }
    reader.readAsDataURL(file)
}


function fileValidation(event, inpField, sizeInKB) {
    if (inpField.files.length > 0) {
        for (const i = 0; i <= inpField.files.length - 1; i++) {

            const fsize = inpField.files.item(i).size;
            const file = Math.round((fsize / 1024));
            if (file >= sizeInKB) {
                alert(`File too big, please select a file less than ${sizeInKB}KB`);
                inpField.value = null
                let previewImg = document.getElementById(inpField.id+'-img')
                previewImg.removeAttribute('src')
                return false
            }
            return true
        }
    }
}

function validateForm (wtf) {
    let y, valid = true;
    y = wtf.querySelectorAll("input, textarea, datalist, select");
    // clean-up classes representing invalid inputs
    document.querySelectorAll('.invalid-feedback').forEach(function (e) {
        e.remove()
    })
    document.querySelectorAll('.is-invalid-radio').forEach(function (e) {
        e.classList.remove('is-invalid-radio')
    })
    // A loop that checks every input field in the current tab:
    for (eInput of y) {
        eInput.classList.remove("is-invalid")
        // If a field is invalid...
        if (!eInput.reportValidity()) {
            eDivInvalid = document.createElement('div')
            eDivInvalid.classList.add('invalid-feedback', 'jf-no-print')
            eDivInvalid.innerText = eInput.validationMessage

            // Radios are multiple input tags - an exception
            if (eInput.type === 'radio' && !eInput.parentElement.parentElement.classList.contains('is-invalid-radio')){
                eInput.parentElement.parentElement.append(eDivInvalid)
                eInput.parentElement.parentElement.classList.add('is-invalid-radio')
                eInput.parentElement.classList.add("is-invalid")
            } else if (eInput.type != 'radio') {
                eInput.parentElement.append(eDivInvalid)
                eInput.classList.add("is-invalid")
            }

            // add an "invalid" class to the tab and step:
            // and set the current valid status to false:
            valid = false;
        }
        $(".date-picker-ne").prop("readonly", false);
    }
    return valid; // return the valid status
};