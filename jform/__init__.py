import os

import flask

__filepath__ = os.path.abspath(__file__)
PATH_WD = os.path.dirname(__filepath__) + '/'
PATH_template = os.path.join(PATH_WD, "templates")
PATH_static   = os.path.join(PATH_WD, "static")


def jforms_serve_static(filepath):
    return flask.send_from_directory(PATH_static, filepath)


def init(app):
    app.jinja_loader.searchpath.append(PATH_template)
    app.add_url_rule(
        '/module/jform/static/<path:filepath>',
        endpoint  = 'jinja-forms-assets',
        view_func = jforms_serve_static
    )